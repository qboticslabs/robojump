# README #

This README guides you to setup Robojump android app on your android studio 

# Contributors #

Android Developer: Lentin Joseph
Graphics: Arun Baby
Algorithm and Suggestions: Dibu Philip and SreeVishnu


### What is this repository for? ###

* A casual android game called Robojump. The game is adapted from Flappy Bird. The character
of the game is Iron Man. Iron man can fly when we tap on the screen. 
Play this exiting Game :)

### How to setup game in Android Studio ###

* Clone the code and import into Android Studio
* Build and push into android device or simulator


### License ###

* MIT 

### Adaptation ###

* Game adapter from existing flappy bird game

### Test ###

* The game robojump tested on Android 5.01 , Device : Galaxy S4